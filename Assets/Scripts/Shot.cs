﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private float shootSpped = 200;
    [SerializeField] Transform explosionPrefab;
    private GameController game;

    void Start()
    {
        game = FindObjectOfType<GameController>();
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, shootSpped * Time.deltaTime, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > 5)
            Destroy(gameObject);
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy" || other.tag == "Shot")
        {
            Transform explo = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            Destroy(explo.gameObject, 0.1f);
            Destroy(other.gameObject);
            Destroy(gameObject);
            if (other.tag == "Enemy")
            {
                game.addScore(10);
                game.removeEnemy();
            }

        }
    }
}
