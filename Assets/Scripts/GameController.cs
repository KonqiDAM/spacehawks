﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    int score;
    int lives;
    [SerializeField] private Text scoreboardText;
    [SerializeField] private Text Textlives;
    [SerializeField] private Text gameOver;
    [SerializeField] private int maxEnemies = 24;
    [SerializeField] private Transform enemyPefab;
    [SerializeField] Transform explosionPrefab;
    private int enemyCount;

    public void addScore(int amount) { score += amount; }
    public void removeLive() { lives--; }

    public void removeEnemy() { enemyCount--; }
    public void Start()
    {
        //this.score = score;
        //this.lives = lives;
        score = 0;
        lives = 5;
        scoreboardText.text = "Score: " + score;
        Textlives.text = "Lives: " + lives;
        gameOver.enabled = false;
        spawnEnemy();
    }

    public void Update()
    {
        scoreboardText.text = "Score: " + score;
        Textlives.text = "Lives: " + lives;

        if (lives <= 0)
        {
            gameOver.enabled = true;
            StartCoroutine(GoMainMenu());
        }

        if (enemyCount <= 0)
            levelUP();

        if (Input.GetKey("i"))
        {
            lives++;
        }
    }
    private void levelUP()
    {
        lives += 3;
        maxEnemies += 2;
        spawnEnemy();
        Time.timeScale = Time.timeScale * 1.1f;

    }
    private void spawnEnemy()
    {
        float startX = -4.2f, startY = 2.3f;

        for (int i = 0; i < maxEnemies; i++)
        {
            Transform e = Instantiate(enemyPefab, transform.position, Quaternion.identity);
            e.Translate(startX, startY, i);
            startX += 0.7f;
            if (startX > 4.5)
            {
                startX = -4.2f;
                startY -= 0.7f;
            }
        }
        enemyCount = maxEnemies;
    }

    IEnumerator GoMainMenu()
    {
        Transform explo = Instantiate(explosionPrefab, FindObjectOfType<SpaceShip>().transform.position, Quaternion.identity);
        Destroy(explo.gameObject, 1f);
        FindObjectOfType<SpaceShip>().enabled = false;
        yield return new WaitForSecondsRealtime(0.7f);
        int pause = 5;
        Time.timeScale = 0.001f;
        yield return new WaitForSecondsRealtime(pause);
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

}
