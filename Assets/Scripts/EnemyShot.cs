﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour
{
    // Start is called before the first frame update
    private GameController game;

    private void Start()
    {
        game = FindObjectOfType<GameController>();
    }
    void Update()
    {
        if (transform.position.y < -5)
            Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Enemy" && other.tag != "Shot")
        {
            game.removeLive();
            Destroy(gameObject);
        }
    }
}
