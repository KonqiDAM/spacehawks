﻿using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]  private float speedX = 3;
    [SerializeField]  private float speedY = 3;
    [SerializeField] Transform prefabEnemyShoot;
    [SerializeField] private float shootSpped = 200;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Shoot());
    }

    // Update is called once per frame
    void Update()
    {
        if ((transform.position.x < -5) || (transform.position.x > 5))
            speedX = -speedX;
        if ((transform.position.y < -3) || (transform.position.y > 3))
            speedY = -speedY;
        transform.Translate(speedX * Time.deltaTime, speedY * Time.deltaTime, 0);
    }

    IEnumerator Shoot()
    {
        float pause = Random.Range(2f, 3f);
        yield return new WaitForSeconds(pause);
        //Debug.Log("Piu Piu");
        Transform shoot = Instantiate(prefabEnemyShoot,
            transform.position,
            Quaternion.identity);

        shoot.gameObject.GetComponent<Rigidbody2D>().velocity =
            new Vector3(0, -shootSpped * Time.deltaTime, 0);

        StartCoroutine(Shoot());
    }

    
}
